#!/usr/bin/python

import xml.etree.ElementTree as ET
import sys
import copy

#len == 3: Xml tag                  conf tag                empty value
#len == 2: Xml&Conf tag             default value

general = [
    ["NotebookModel",               "Model",                "NOMODEL"],
    ["EcPollInterval",              "3000"],
    #["ReadWriteWords",              "false"],
    ["CriticalTemperature",         "50"],
    ["Fans",                        ""],
    ["ControlRegisters",            ""],
]

fan = [
    ["ReadRegister",                "-1"],
    ["WriteRegister",               "-1"],
    ["MinSpeedValue",               "-1"],
    ["MaxSpeedValue",               "-1"],
    #["IndependentReadMinMaxValues", "false"],
    #["MinSpeedValueRead",          "0"],
    #["MaxSpeedValueRead",          "0"],
    ["ResetRequired",               "true"],
    ["FanSpeedResetValue",          "-1"],
    #["FanDisplayName",              "NOFANNAME"],
    ["Rules",                       ""],
]

controlReg = [
    ["WriteOccasion",               "OnInitialization"],
    ["Register",                    "-1"],
    ["Value",                       "-1"],
    ["ResetRequired",               "-1"],
    ["ResetValue",                  "-1"],
]

fanRule = [
    ["UpThreshold",                 "UpTemperature",                "-1"],
    ["DownThreshold",               "DownTemperature",              "-1"],
    ["FanSpeed",                    "Speed",                        "-1"],
]

def main(inFileName):
    tree = ET.parse(inFileName)
    root = tree.getroot()
    
    fans = []
    controlRegs = []
    fanRules = []

    fanConfigs = root.find("FanConfigurations")
    controlRegsConfigs = root.find("RegisterWriteConfigurations")
    fanRuleConfigs = []
    

    for fanConfig in fanConfigs:
        tth = fanConfig.find("TemperatureThresholds")
        #print(tth)
        fanRuleConfigs.append(tth.findall("TemperatureThreshold"))
    
    generalFans = general[len(general)-2]
    generalControlRegs = general[len(general)-1]

    for generalTag in general:
        
        for tag in root.findall(generalTag[0]):
            if len(tag.text) != 0:
                if len(generalTag) == 3:
                    generalTag[2] = tag.text
                else: 
                    generalTag[1] = tag.text


    for controlRegConfigsTag in controlRegsConfigs:
        newControlReg = copy.deepcopy(controlReg)
        empty = False
        for regTag in newControlReg:
            for tag in controlRegConfigsTag:
                if regTag[0] == tag.tag:
                    if len(tag.text) != 0:
                        if len(regTag) == 3:
                            regTag[2] = tag.text
                        else: 
                            regTag[1] = tag.text
                    else: 
                        empty = True
                        break
        if empty == False:
            controlRegs.append(newControlReg)
            generalControlRegs[1] += "reg" + str(len(controlRegs)) + " "
    
    fanRulesI = 0
    
    for i,fanConfigsTag in enumerate(fanConfigs):
        newFan = copy.deepcopy(fan)
        empty = False
        for fanTag in newFan:
            for tag in fanConfigsTag:
                if fanTag[0] == tag.tag:
                    if len(tag.text) != 0:
                        if len(fanTag) == 3:
                            fanTag[2] = tag.text
                        else: 
                            fanTag[1] = tag.text
                    else: 
                        empty = True
                        break
        
        for hz in range(len(fanRuleConfigs[i])):
            newFan[len(newFan)-1][1] += "fanRule" + str(fanRulesI + 1) + " "
            fanRulesI += 1

        if empty == False:
            fans.append(newFan)
            generalFans[1] += "fan" + str(len(fans)) + " "
    
    for fanRulesConfigsTag in fanRuleConfigs:
        for fanRuleConfigsTag in fanRulesConfigsTag:
            newFanRule = copy.deepcopy(fanRule)
            empty = False
            for frTag in newFanRule:
                for tag in fanRuleConfigsTag:
                    if frTag[0] == tag.tag:
                        if len(tag.text) != 0:
                            if len(frTag) == 3:
                                frTag[2] = tag.text
                            else: 
                                frTag[1] = tag.text
                        else: 
                            empty = True
                            break
            if empty == False:
                fanRules.append(newFanRule)
    
    # Printing
    print("[General]")

    for tag in general:
        printTag(tag)

    print("") # Just for good look

    i = 0
    for fan_i in fans:
       i += 1 
       print("[fan" + str(i) + "]")
       for tag in fan_i:
           printTag(tag)
       print("")

    i = 0
    for reg_i in controlRegs:
        i += 1
        print("[reg" + str(i) + "]")
        for tag in reg_i:
            printTag(tag)
        print("")
    
    i = 0
    for fanRule_i in fanRules:
        i += 1
        print("[fanRule" + str(i) + "]")
        for tag in fanRule_i:
            printTag(tag)
        print("")



def printTag(tag):
    if len(tag) == 3:
        print(tag[1] + " = " + tag[2])
    else: 
        print(tag[0] + " = " + tag[1])


if len(sys.argv) == 2:
    main(sys.argv[1])
else:
    print("Usage: " + sys.argv[0] + " [input.xml]")  
