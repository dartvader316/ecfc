#include <stdlib.h>
#include <err.h>
#include <sys/io.h>
#include "ec.h"

#define RW_TIMEOUT 500


#ifdef __SB_CHECK 

#include <efivar.h>

#endif

#ifdef __linux__
enum {
	EC_SIZE = 0xff,
	// Offsets
	EC_START = 0x60,
	EC_SC	= 0x66,
	EC_DATA = 0x62,
	
	//Status
	OBF = 0x01,
	IBF = 0x02,
	CMD = 0x08,
	BURST = 0x10,
	SCI_EVT = 0x20,
	SMI_EVT = 0x40,
	
	// Commands
	RD_EC = 0x80,	//Read 
	WR_EC = 0x81,	//Write
	BE_EC = 0x82,	//Burst Enable 
	BD_EC = 0x83,	//Burst Disable
	QR_EC = 0x84,	//Query
};

#endif

#ifdef __SB_CHECK 
// Untested by me so disabled by default
// Requires efivar lib


static int sb_state(){
	uint8_t *data = NULL;
	size_t data_size;
	uint32_t attributes;
	int32_t secureboot = -1;

	if(efi_get_variable(efi_guid_global,"SecureBoot",&data,&data_size,&attributes) < 0) {
		return -2;
	}

	if(data_size != 1) {
		return -3;
	}
	if(data_size == 4 || data_size == 2 || data_size == 1) {
		secureboot = 0;
		memcpy(&secureboot, data, data_size);
	}
	free(data);
	
	return secureboot;
}

#else

static int sb_state(){
	return 0;
}

#endif

int init_ec(){
	if(sb_state()==1){
		return -2;
	}
	return ioperm(EC_START,EC_SIZE,1);
}

void close_ec(){
	ioperm(EC_START,EC_SIZE,0);
}

int wait_read_ec(){
	int timeout = RW_TIMEOUT;
	uint8_t s;
	while(timeout > 0){
		s = inb_p(EC_SC);
		
		if(s & OBF)
			return 0;
		
		timeout--;
	}
	return 1;
}

int wait_write_ec(){
	int timeout = RW_TIMEOUT;
	
	uint8_t s;
	while(timeout > 0){
		s = inb_p(EC_SC);
		if(!(s & IBF))
			return 0;
		
		timeout--;
	}
	return 1;
}

void prepaire_ec(uint8_t cmd,uint8_t reg){
	if(wait_write_ec())
		goto out;
	outb_p(cmd,EC_SC);
	
	if(wait_write_ec())
		goto out;
	outb_p(reg,EC_DATA);
	
	return;
	out:
	err(EXIT_FAILURE, "Error waiting for EC prepaired");
}

uint8_t read_ec(uint8_t offset){

	prepaire_ec(RD_EC,offset);
	
	if(!wait_read_ec()){ 
		return inb_p(EC_DATA);
	}

	err(EXIT_FAILURE, "Error waiting for EC read");
}


void write_ec(uint8_t offset,uint8_t value){
	
	prepaire_ec(WR_EC,offset);
	
	if(!wait_write_ec()){ 
		outb_p(value,EC_DATA);
		return;
		
	}

	err(EXIT_FAILURE, "Error waiting for EC write");

}
