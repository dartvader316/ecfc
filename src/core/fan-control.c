#include "fan-control.h"

#include <err.h>
#include <stdlib.h>

static inline uint8_t percent2fan_speed(const device_config* config,size_t fan_id,int value);
static inline uint8_t fan_speed2percent(const device_config* config,size_t fan_id,int value);

void set_rule_by_temp(const device_config* config,int temp){
	if(is_manual(config,FAN_ALL)){ 

		if(temp >= config->critical_temp){
			set_fan_speed_percent(config,FAN_ALL,100);
			return;
		}

		for(size_t i = 0;i<config->fans_size;i++){
			const size_t current_fan_rules_size = config->fans[i].fan_rules_size;
			const ssize_t current_rule = config->fans[i].current_rule;
			
			size_t rule = 0;

			if(current_rule>=0){
				rule = (size_t) current_rule;
			}
			if(rule < current_fan_rules_size-1 && temp >= config->fans[i].fan_rules[rule+1].up_temp){
				while(rule < current_fan_rules_size-1 && temp >= config->fans[i].fan_rules[rule+1].up_temp)
					rule++;
			}else{
				while(rule > 0 && temp <= config->fans[i].fan_rules[rule].down_temp)
						rule--;
				
			}
			config->fans[i].current_rule = (ssize_t)rule;
			set_fan_speed_percent(config,(ssize_t)i,config->fans[i].fan_rules[config->fans[i].current_rule].speed);

		}
	}

}

void set_manual(const device_config* config,ssize_t fan_id){
	size_t i = (size_t) fan_id;
	if(fan_id == FAN_ALL){ 
		for(i = 0;i<config->control_regs_size;i++){
			write_ec(config->control_regs[i].reg,config->control_regs[i].value);
		}

		for(i = 0;i<config->fans_size;i++){
			if(config->fans[i].reset_required)
				write_ec(config->fans[i].write_reg,config->fans[i].reset_value);
			
			config->fans[i].current_rule = 0;
		}
	}else{
		if(fan_id<0 || i >= config->control_regs_size)
			err(EXIT_FAILURE, "Wrong fan : %ld",fan_id);
		else
			write_ec(config->control_regs[i].reg,config->control_regs[i].value);
		
	}
}

void set_auto(const device_config* config,ssize_t fan_id){
	size_t i = (size_t) fan_id;
	if(fan_id == FAN_ALL){ 
		for(i = 0;i<config->control_regs_size;i++)
			write_ec(config->control_regs[i].reg,config->control_regs[i].reset_value);

	}else{
		if(fan_id<0 || i >= config->control_regs_size)
			err(EXIT_FAILURE, "Wrong fan : %ld",fan_id);
		else
			write_ec(config->control_regs[i].reg,config->control_regs[i].reset_value);
		
	}
}

bool is_manual(const device_config* config,ssize_t fan_id){
	size_t i = (size_t) fan_id;
	
	if(fan_id == FAN_ALL){ 
		bool ret = true;
		for(i = 0;i<config->control_regs_size;i++){
			if(read_ec(config->control_regs[i].reg) != config->control_regs[i].value)
				ret = false;

		}
		return ret;
	}else{
		if(fan_id<0 || i >= config->control_regs_size)
			err(EXIT_FAILURE, "Wrong fan : %ld",fan_id);
		else
			return read_ec(config->control_regs[i].reg);
		
	}

	return false;
}

void set_fan_speed(const device_config* config,ssize_t fan_id,uint8_t value){
	size_t i = (size_t) fan_id;
	if(fan_id == FAN_ALL){ 
		for(i = 0;i<config->fans_size;i++){
			write_ec(config->fans[i].write_reg,value);
		}

	}else{
		if(fan_id<0 || i >= config->fans_size)
			err(EXIT_FAILURE, "Wrong fan : %ld",fan_id);
		else
			write_ec(config->fans[i].write_reg,value);
		
	}

}

uint8_t get_fan_speed(const device_config* config,ssize_t fan_id){
	size_t i = (size_t) fan_id;
	
	if(fan_id<0 || i >= config->fans_size)
		err(EXIT_FAILURE, "Wrong fan : %ld",fan_id);
	else
		return read_ec(config->fans[i].read_reg);
		

}

static inline uint8_t percent2fan_speed(const device_config* config,size_t fan_id,int value){
	
	const int min = config->fans[fan_id].min_speed;
	const int max = config->fans[fan_id].max_speed;
	
	if(max==min) return config->fans[fan_id].max_speed;

	int res = 0;
	if(max > min){
		res = (int) (((max - min)/(double)100) * value) + min;
	
	}else{
		value = 100 - value;
		res = (int) (((min - max)/(double)100) * value) + max;
	}

	if(res>=0&&res<256){
		return (uint8_t)res;
	}

	return config->fans[fan_id].max_speed;
}

static inline uint8_t fan_speed2percent(const device_config* config,size_t fan_id,int value){
	
	const int min = config->fans[fan_id].min_speed;
	const int max = config->fans[fan_id].max_speed;
	
	if(max==min) return 100;

	int res = 0;
	if(max > min){
		res = (int) ((value - min) * 100)/(max-min);
	}else{
		res = (int) (100 - ((value - max) * 100)/(double)(min-max));
	}

	if(res>=0&&res<=100){
		return (uint8_t)res;
	}

	return 100;

}

void set_fan_speed_percent(const device_config* config,ssize_t fan_id,int value){
	size_t i = (size_t) fan_id;
	if(value>100 || value<0) err (EXIT_FAILURE, "Wrong percentage: %d", value);
	
	uint8_t speed = 0;

	if(fan_id == FAN_ALL){ 
		for(i = 0;i<config->fans_size;i++){
			speed = percent2fan_speed(config,i,value);
			set_fan_speed(config,(ssize_t)i,speed);
		}
	}else{
		if(fan_id<0 || i >= config->fans_size)
			err(EXIT_FAILURE, "Wrong fan : %ld",fan_id);
		else{
			speed = percent2fan_speed(config,i,value);
			set_fan_speed(config,(ssize_t)i,speed);
		}
	}

}

uint8_t get_fan_speed_percent(const device_config* config,ssize_t fan_id){
	size_t i = (size_t) fan_id;
	
	if(fan_id<0 || i >= config->fans_size)
		err(EXIT_FAILURE, "Wrong fan : %ld",fan_id);
	else
		return fan_speed2percent(config,i,get_fan_speed(config,fan_id));
		
}
