#define _POSIX_C_SOURCE 200809L

#include <stdio.h>
#include <unistd.h>
#include <limits.h>
#include <string.h>
#include <stdlib.h>
#include <dirent.h>
#include <math.h>
#include <stdbool.h>
#include <err.h>
#include "temp.h"


// https://www.kernel.org/doc/html/latest/hwmon/
static const char* hwmon_names[] = {
	"coretemp",
	"k8temp",
	"k10temp",
};

static const char* thermal_zones_types[] = {
	"x86_pkg_temp"

};

struct def_temp_source {
	const char* path;
	const char* folder_prefix;

	const char* file_prefix;
	const char* file_postfix;

	const char* file_validator;
	const char** validator_values;
	const size_t validator_values_size;
};

static const struct def_temp_source def_temp_sources[] = {
	{"/sys/class/thermal", 	"thermal_zone",	"temp",NULL,	"type",	thermal_zones_types,sizeof(thermal_zones_types)/sizeof(char*)},
	{"/sys/class/hwmon", 	"hwmon",		"temp","_input",	"name",	hwmon_names,sizeof(hwmon_names)/sizeof(char*)},
};

void free_temp_sources(temp_sources* temp_s){
	for(size_t i = 0;i<temp_s->size;i++){
		free(temp_s->paths[i]);
	}

	free(temp_s->paths);
}

void detect_temp_sources(temp_sources* temp_s){
	size_t mem_size = 128;
	temp_s->paths = malloc(mem_size*sizeof(char*));
	
	size_t s = 0;
	
	DIR* dp; 
	DIR* dp2; 
	
	struct dirent* dir;
	struct dirent* dir2;
	
	FILE* fp;
	
	char* path = calloc(PATH_MAX,sizeof(char));
	char* buffer = calloc(4096, sizeof(char));

	for(size_t i = 0;i<sizeof(def_temp_sources)/sizeof(struct def_temp_source);i++){
		const struct def_temp_source dts = def_temp_sources[i];
		dp = opendir(dts.path);

		while((dir = readdir(dp))){

			if(!strncmp(dir->d_name,dts.folder_prefix,strlen(dts.folder_prefix))){
				memset(path,0,PATH_MAX);
				snprintf(path,PATH_MAX-1,"%s/%s",dts.path,dir->d_name);
				dp2 = opendir(path);
				while((dir2 = readdir(dp2))){
					if(dts.file_prefix == NULL || !strncmp(dir2->d_name,dts.file_prefix,strlen(dts.file_prefix))){
						const size_t l = strlen(dir2->d_name);
						const size_t l2 = (dts.file_postfix != NULL) ? strlen(dts.file_postfix) : 0;
						
						if(dts.file_postfix == NULL || !strncmp(dir2->d_name + l - l2 ,dts.file_postfix,l2)){
							bool valid = false;
							if(dts.file_validator == NULL){
								valid = true;
							}else{
								snprintf(path,PATH_MAX-1,"%s/%s/%s",dts.path,dir->d_name,dts.file_validator);
								fp = fopen(path,"r");
								if(fp){
									int readed = fscanf(fp,"%s",buffer);
									if(readed > 0){
										const size_t size = (size_t)readed;
										for(size_t j = 0;j<dts.validator_values_size;j++){
											if(!strncmp(buffer,dts.validator_values[j],size)){
												valid = true;
											}
										}
									}
									fclose(fp);
								}
							}
							if(valid){
								snprintf(path,PATH_MAX-1,"%s/%s/%s",dts.path,dir->d_name,dir2->d_name);
								temp_s->paths[s] = strdup(path);
								s++;

								if(s>=mem_size){
									mem_size *=2;
									temp_s->paths = realloc(temp_s->paths,mem_size*sizeof(char*));
									
									if(!temp_s->paths){
										err(
										EXIT_FAILURE,
										"Cant allocate more memory for temperature sources"
										);
									}
								}
							}
						}
					}

				}
				closedir(dp2);
			}
		}

		closedir(dp);
	}
	free(path);
	free(buffer);
	temp_s->size = s;
}

static inline int temp_from_file(const char* path){
	int ret = -1;

	FILE* fp = fopen(path,"r");
	if(fp){
		fscanf(fp,"%2d",&ret);
		fclose(fp);
		//printf("%s: \t%d \n",path,ret);
	}
	return ret;
 }


int get_max_temp(const temp_sources* temp_s){
	int temp = -1;			
	for(size_t i = 0;i<temp_s->size;i++){
		int btemp = temp_from_file(temp_s->paths[i]);
		if(btemp>temp) temp = btemp;
	}
	return temp;
	
}

int get_avg_temp(const temp_sources* temp_s){
	
	int tempsum = 0;
	size_t temps = 0;

	for(size_t i = 0;i<temp_s->size;i++){
		tempsum += temp_from_file(temp_s->paths[i]);
		temps++;
		
	}
	
	return tempsum/(int)temps;
	
}


int get_temp(const temp_sources* temp_s){
	return get_avg_temp(temp_s);
}
