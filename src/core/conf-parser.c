#define _POSIX_C_SOURCE 200809L

#include "conf-parser.h"

#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <stddef.h>
#include <err.h>
#include <limits.h>

/*
 *
 * 	WARNING
 *
 * 	IF YOU DONT WANT ANY MENTAL PROBLEMS PLEASE DO NOT READ THE CODE BELOW
 *
 *
 *
 */

#define LINE_BUFFER_SIZE 4096

enum parsable_type {
	PTYPE_INT32,
	PTYPE_UINT8,
	PTYPE_STR,
	PTYPE_BOOL,
	PTYPE_WOENUM, // Write occasion enum
	PTYPE_ELARR, // Element array
};

enum el_arr_type {
	ELARR_FANS,
	ELARR_CREGS,
	ELARR_FAN_RULES,
};

typedef struct {
	const char* var_name;
	int ptype;
	size_t struct_offset;
} parsable_field;

static const parsable_field device_config_parsable_fields[] = {
	{"Model",				PTYPE_STR,		offsetof(device_config,model)},
	{"EcPollInterval",		PTYPE_INT32,	offsetof(device_config,ec_poll_interval)},
	{"CriticalTemperature",	PTYPE_UINT8,	offsetof(device_config,critical_temp)},
	{"Fans",				PTYPE_ELARR,	ELARR_FANS},
	{"ControlRegisters",	PTYPE_ELARR,	ELARR_CREGS},

	//{"ReadWriteWords",	PTYPE_BOOL,		offsetof(device_config,)
};

static const parsable_field fan_parsable_fields[] = {
	{"ReadRegister",		PTYPE_UINT8,	offsetof(fan,read_reg)},
	{"WriteRegister",		PTYPE_UINT8,	offsetof(fan,write_reg)},
	{"MinSpeedValue",		PTYPE_UINT8,	offsetof(fan,min_speed)},
	{"MaxSpeedValue",		PTYPE_UINT8,	offsetof(fan,max_speed)},
	{"ResetRequired",		PTYPE_BOOL,		offsetof(fan,reset_required)},
	{"FanSpeedResetValue",	PTYPE_UINT8,	offsetof(fan,reset_value)}, 
	{"Rules",				PTYPE_ELARR,	ELARR_FAN_RULES},
	//{"IndependentReadMinMaxValues",PTYPE_BOOL 
};

static const parsable_field fan_rule_parsable_fields[] = {
	{"UpTemperature",	PTYPE_INT32,	offsetof(fan_rule,up_temp)},
	{"DownTemperature",	PTYPE_INT32,	offsetof(fan_rule,down_temp)},	
	{"Speed",			PTYPE_UINT8,	offsetof(fan_rule,speed)},
};

static const char wo_enum[] = {
	"OnInitialization",

};

static const parsable_field creg_parsable_fields[] = {
	//WriteOccasion = OnInitialization
	
	{"WriteOccasion",	PTYPE_WOENUM,	offsetof(control_reg,write_occasion)},
	{"Register",		PTYPE_UINT8,	offsetof(control_reg,reg)},	
	{"Value",			PTYPE_UINT8,	offsetof(control_reg,value)},
	{"ResetRequired",	PTYPE_BOOL,		offsetof(control_reg,reset_required)},
	{"ResetValue",		PTYPE_UINT8,	offsetof(control_reg,reset_value)},
};


enum section {
	NO_SECTION,
	SECTION_GENERAL,
	SECTION_FAN,
	SECTION_FAN_RULE,
	SECTION_CREG,
};


ssize_t parse_el_arr(const char* str, char*** ret){
	if(str){
		size_t size = 256;
		char** arr = calloc(size, sizeof(char*));
		const char* sp;
		size_t i = 0;
		for(;(sp = strchr(str,' '))&&i<size-1;i++){
			ssize_t pos = (sp-str);
			if(pos<0) break;

			arr[i] = strndup(str,(size_t)pos);
			str = sp + 1;

			for(;str[0]==' '&&str[0]!='\0';str++);
		}
		if(str[0]>' '&&i<size-1){	
			arr[i++] = strdup(str);
		}
		*ret = arr;
		
		return (ssize_t)i;
	}
	return -1;
}

int parse_val(const char* str,void* struct_p,int ptype,size_t offset){
		void* const cp = ((char*)struct_p) + offset;
		
		uint8_t* const uint8_p = cp;
		int32_t* const int_p = cp;
		char** const str_p = cp;
		bool* const bool_p = cp;
		
		device_config* const config = struct_p;
		fan* const fan_struct = struct_p;
		
		char** el_arr;
		size_t size;
		ssize_t rsize;
		switch(ptype){
			case PTYPE_INT32:
				*int_p = (int32_t) strtol(str,NULL,10);
				break;
			case PTYPE_UINT8:
				*uint8_p = (uint8_t) strtol(str,NULL,10);
				break;
			case PTYPE_STR:
				*str_p = strdup(str);
				break;
			case PTYPE_BOOL:
				*bool_p = (bool) !strcmp(str,"true");
				if(!strcmp(str,"false") == *bool_p)
					return 1;
				break;
			case PTYPE_ELARR:
				switch(offset){
					case ELARR_FANS:
						rsize = parse_el_arr(str,&el_arr);
						if(rsize<0) return -1;
						size = (size_t) rsize;
						config->fans_size = size;
						config->fans = calloc(size,sizeof(fan));
						for(size_t i = 0;i<size;i++){
							config->fans[i] = (fan) {
								.name = strdup(el_arr[i]),
								.read_reg = 0,
								.write_reg = 0,
								.reset_required = false,
								.min_speed = 0,
								.max_speed = 0,
								.current_rule = -1,
								.fan_rules_size = 0
							};
							free(el_arr[i]);
						}
						free(el_arr);
						break;
					case ELARR_CREGS:
						rsize = parse_el_arr(str,&el_arr);
						if(rsize<0) return -1;
						size = (size_t) rsize;
						
						config->control_regs_size = size;
						config->control_regs = calloc(size,sizeof(fan));
						for(size_t i = 0;i<size;i++){
							config->control_regs[i] = (control_reg){
								.name = strdup(el_arr[i]),
								.reg = 0,
								.write_occasion = -1,
								.reset_required = false
							};
							free(el_arr[i]);
						}
						free(el_arr);
						break;
					case ELARR_FAN_RULES:
						rsize = parse_el_arr(str,&el_arr);
						if(rsize<0) return -1;
						size = (size_t) rsize;
						
						fan_struct->fan_rules_size = size;
						fan_struct->fan_rules = calloc(size,sizeof(fan_rule));
						for(size_t i = 0;i<size;i++){
							fan_struct->fan_rules[i] = (fan_rule){
								.name = strdup(el_arr[i]),
								.up_temp = -1,
							    	.down_temp = -1,
							    	.speed = 100,
							};
							free(el_arr[i]);
						}
						free(el_arr);
						break;
					default:
						break;
				}
				break;

			case PTYPE_WOENUM:
				for(size_t i = 0;i<sizeof(wo_enum)/sizeof(wo_enum[0]);i++){
					if(!strcmp(wo_enum,str))
						*int_p = (int32_t) i;
				}
				break;
		}

		
		return 0;
}

void clear_str(char* s) {
	const size_t len = strlen(s);
	for(size_t i = 0;s[0] == ' ';i++){
		for(size_t j = 0;j<len;j++){
			s[j] = s[j+1];
			s[len-i-1] = '\0';
		}
	}
	for(size_t i = len;s[strlen(s)-1] == ' ';i--){
			s[i] = '\0';
	}
}

static inline ssize_t strposch(const char* s, char ch){
	char* p = strchr(s,ch);
	if(p){
		return  p-s;
	}else{
		return -1;
	}
}

int parse_line(char* str,char* var_buffer,char* val_buffer){
	if(str&&var_buffer&&val_buffer){
		
		memset(var_buffer,0,strlen(var_buffer));
		memset(val_buffer,0,strlen(val_buffer));
		
		ssize_t rch_i = strposch(str,'=');
		ssize_t rend_i = (ssize_t)strlen(str) - 1;
		
		if(rch_i>0&&rend_i>0){
			const size_t ch_i = (size_t) rch_i;
			const size_t end_i = (size_t) rend_i;
			strncpy(var_buffer,str,ch_i);
			strncpy(val_buffer,str + ch_i + 1,end_i);
			clear_str(var_buffer);
			clear_str(val_buffer);
		
			return 0;
		}
		return 1;
	}
	return 0;
}

int general_parse_line(char* str,device_config* config,char* var_buffer,char* val_buffer){
	if(!parse_line(str,var_buffer,val_buffer)){

		for(size_t i = 0;i<sizeof(device_config_parsable_fields)/sizeof(parsable_field);i++){
			if(!strcmp(device_config_parsable_fields[i].var_name,var_buffer)){
				int ptype = device_config_parsable_fields[i].ptype;
				size_t offset = device_config_parsable_fields[i].struct_offset;
				return parse_val(val_buffer,config,ptype,offset);
			}
		}
		return 1;
	}
	return 0;
}

int fan_parse_line(char* str,device_config* config,size_t fan_id, char* var_buffer,char* val_buffer){
	if(!parse_line(str,var_buffer,val_buffer)){
		for(size_t i = 0;i<sizeof(fan_parsable_fields)/sizeof(parsable_field);i++){
			if(!strcmp(fan_parsable_fields[i].var_name,var_buffer)){
				int ptype = fan_parsable_fields[i].ptype;
				size_t offset = fan_parsable_fields[i].struct_offset;
				return parse_val(val_buffer,config->fans + fan_id,ptype,offset);
			}
		}
		return 1;
	}
	return 0;
}

int fan_rule_parse_line(char* str,device_config* config,size_t fan_id, size_t fan_rule_id, char* var_buffer,char* val_buffer){
	if(!parse_line(str,var_buffer,val_buffer)){

		for(size_t i = 0;i<sizeof(fan_rule_parsable_fields)/sizeof(parsable_field);i++){
			if(!strcmp(fan_rule_parsable_fields[i].var_name,var_buffer)){
				int ptype = fan_rule_parsable_fields[i].ptype;
				size_t offset = fan_rule_parsable_fields[i].struct_offset;
				return parse_val(val_buffer,(config->fans + fan_id)->fan_rules + fan_rule_id,ptype,offset);

			}
		}
		return 1;
	}
	return 0;
}

int creg_parse_line(char* str,device_config* config,size_t reg, char* var_buffer,char* val_buffer){
	if(!parse_line(str,var_buffer,val_buffer)){

		for(size_t i = 0;i<sizeof(creg_parsable_fields)/sizeof(parsable_field);i++){
			if(!strcmp(creg_parsable_fields[i].var_name,var_buffer)){
				int ptype = creg_parsable_fields[i].ptype;
				size_t offset = creg_parsable_fields[i].struct_offset;
				return parse_val(val_buffer,config->control_regs + reg,ptype,offset);
			}
		}
		return 1;
	}
	return 0;
}

typedef struct {
	size_t fan_index;
	size_t fan_rule_index;
} pending_fan_info;

int section_parse_line(const char* str,const device_config* config,size_t* index,pending_fan_info* f_buffer, size_t f_buffer_size,size_t* pf_index_buffer){
	static const char general_section[] = "General";

	if(str){
		ssize_t rstart_i = strposch(str,'[');
		ssize_t rend_i = strposch(str,']') -1;

		if(rstart_i>=0 && rend_i>0){
			rstart_i++;
			const size_t start_i = (size_t) rstart_i;
			const size_t end_i = (size_t) rend_i;

			if(!strncmp(str+start_i,general_section,end_i))
				return SECTION_GENERAL;

			size_t pf_index = 0;
			if(config->fans && config->fans_size){
				for(size_t i = 0;i<config->fans_size;i++){
					const char* fan_name = config->fans[i].name;
					const size_t fan_name_len = strlen(fan_name);
					if(!strncmp(str+start_i,fan_name,fan_name_len)){
						if(index) *index = i;
						return SECTION_FAN;

					}
					
					if(config->fans[i].fan_rules && config->fans[i].fan_rules_size){
						for(size_t j = 0;j<config->fans[i].fan_rules_size;j++){
							const char* fan_rule_name = config->fans[i].fan_rules[j].name;
							const size_t fan_rule_name_len = strlen(fan_rule_name);

							if(!strncmp(str+start_i,fan_rule_name,fan_rule_name_len)){
								f_buffer[pf_index].fan_index = i;
								f_buffer[pf_index].fan_rule_index = j;

								pf_index++;

								if(pf_index >= f_buffer_size){
									return SECTION_FAN_RULE;
								}
							}

						}
					}
				}
			}

			if(pf_index>0){
				*pf_index_buffer = pf_index;
				return SECTION_FAN_RULE;
			}

			if(config->control_regs && config->control_regs_size){
				for(size_t i = 0;i<config->control_regs_size;i++){
					const char* creg_name = config->control_regs[i].name;
					const size_t creg_name_len = strlen(creg_name);

					if(!strncmp(str+start_i,creg_name,creg_name_len)){
						if(index) *index = i;
						return SECTION_CREG;
					}	
				}
			}
		}
	}

	return NO_SECTION;
}

void sort_fan_rules(fan_rule* arr,size_t size){
	fan_rule buff;
	
	for(size_t i = 1;i<size;i++){
		
		buff = arr[i];

		ssize_t j = ((ssize_t)i)-1;
		
		for(;j>=0 && arr[j].up_temp > buff.up_temp;j--){
			arr[j+1] = arr[j];
		}

		arr[j+1] = buff;
	}

}

int parse(const char* file_path,device_config* config){

	FILE* fp = fopen(file_path,"r");
	if(fp){
		
		int error = 0;
		device_config ret = {
			.ec_poll_interval = 3000,
			.critical_temp = 0,
			.fans_size = 0,
			.control_regs_size = 0,
		};

		
		int section = NO_SECTION;
		
		char* line = calloc(LINE_BUFFER_SIZE,sizeof(char));
		char* var_buffer = calloc(LINE_BUFFER_SIZE,sizeof(char));
		char* val_buffer = calloc(LINE_BUFFER_SIZE,sizeof(char));
		
		size_t index = 0;
		//size_t index2 = 0;
		
		// FIXME
		// Workaround to use same fan rules in mutliple fans.
		// Fan control registers cant be more than EC size (256), so limit it to that number;
		//

		const size_t pf_size = 256;
		pending_fan_info* pf = calloc(pf_size,sizeof(pending_fan_info));
		size_t pf_index = 0;

		for(size_t line_i = 1;fgets(line,LINE_BUFFER_SIZE,fp);line_i++){
			if(strposch(line,'[')>=0) section = NO_SECTION;
			
			{
			ssize_t pos = -1;
			if((pos = strposch(line,'#'))>=0) 
				line[pos] = '\0';
			}

			switch(section){
				case SECTION_GENERAL:
					error = general_parse_line(line,&ret,var_buffer,val_buffer);
					break;
				case SECTION_FAN:
					error = fan_parse_line(line,&ret,index,var_buffer,val_buffer);
					break;
				case SECTION_FAN_RULE:
					if(pf_index>0){
						for(size_t i = 0;i<pf_index;i++){
							pending_fan_info* const pf_buff = pf + i;
							//FIXME multierror handle
							error = fan_rule_parse_line(line,&ret,pf_buff->fan_index,pf_buff->fan_rule_index,var_buffer,val_buffer);
						}
					}
					break;
				case SECTION_CREG:
					error = creg_parse_line(line,&ret,index,var_buffer,val_buffer);
					break;
				case NO_SECTION:

					memset(pf,0,pf_index);
					pf_index = 0;

					section = section_parse_line(line,&ret,&index,pf,pf_size,&pf_index);
					if(section<0){
						error = section;
					}
					break;
				default:
					if(line_i<INT_MAX)
						error = (int) line_i;
			}

			if(error){
				if(line_i<INT_MAX)
					error = (int)line_i;

				break;
			}

		}
		*config = ret;
		
		for(size_t i = 0;i<config->fans_size;i++){
			fan_rule* const arr = config->fans[i].fan_rules;
			if(arr){
				sort_fan_rules(arr,config->fans[i].fan_rules_size);
			}
		}

		free(line);
		free(var_buffer);
		free(val_buffer);
		
		free(pf);

		fclose(fp);
		return error;
	}

	return -1;

}
