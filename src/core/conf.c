#include "conf.h"

#include <stdlib.h>

void free_fan_rule(fan_rule* fr){
	free(fr->name);
}
void free_fan(fan* f){
	free(f->name);
	
	for(size_t i = 0;i<f->fan_rules_size;i++){
		free_fan_rule(f->fan_rules + i);
	}
	free(f->fan_rules);
}

void free_control_reg(control_reg* reg){
	free(reg->name);
}
void free_device_config(device_config* c){
	
	free(c->model);
	for(size_t i = 0;i<c->fans_size;i++){
		free_fan(c->fans + i);
	}
	for(size_t i = 0;i<c->control_regs_size;i++){
		free_control_reg(c->control_regs + i);
	}
	free(c->control_regs);
	free(c->fans);
}

