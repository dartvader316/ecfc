#define _XOPEN_SOURCE 500

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <signal.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <time.h>
#include <errno.h>

#include "conf.h"
#include "conf-parser.h"
#include "fan-control.h"
#include "temp.h"

void initd();
void loopd();
void exitd(int s);

static volatile sig_atomic_t loopd_stop = 0;

device_config* global_config;
temp_sources* global_temp_sources;
int ec_status = -1;

int pid_fd = -1;

void initd(){
	static const char* pid_file_path = "/run/ecfcd.pid";

	//signal(SIGCHLD, SIG_IGN);
	//signal(SIGHUP, SIG_IGN);
	
	
	pid_t pid;
	pid = fork();
	
	if(pid>0)
		exit(EXIT_SUCCESS);
	
	if(setsid()<0||pid<0)
		exit(EXIT_FAILURE);

	pid = fork();
	
	if(pid<0)
		exit(EXIT_FAILURE);

	if(pid>0)
		exit(EXIT_SUCCESS);
	
	umask(0);
	chdir("/");
	//for(int fd=sysconf(_SC_OPEN_MAX);fd>0;fd--)
	//	close(fd);

	pid_fd = open(pid_file_path, O_RDWR|O_CREAT, 0640);
	if(pid_fd<0) {
		exit(EXIT_FAILURE);
	}
	if(lockf(pid_fd,F_TLOCK,0)<0) {
		exit(EXIT_FAILURE);
	}
	
	#define STR_SIZE 256
	char str[STR_SIZE];
	snprintf(str,STR_SIZE,"%d\n", getpid());
	write(pid_fd,str,strlen(str));
	#undef STR_SIZE
	
	fprintf(stderr,"Daemon successfully initialised with PID %d \n",getpid());
}

void loopd(){
	if(!global_config){
		fprintf(stderr,"LOGIC ERROR - loopd started with empty config \n");
		return;
	}
	if(!global_temp_sources){
		fprintf(stderr,"LOGIC ERROR - loopd started with empty global_temp_sources \n");
		return;
	}
    	fprintf(stderr,"Setting fans to manual... \n");
	set_manual(global_config,FAN_ALL);
	
	const struct timespec ts = {
		.tv_sec = global_config->ec_poll_interval / 1000,
		.tv_nsec = (global_config->ec_poll_interval % 1000) * 1000000,
	
	};
	
	int temp = 0;
	
    	fprintf(stderr,"Starting main loop with interval: %ld seconds and %ld nanoseconds \n",ts.tv_sec,ts.tv_nsec);
	while(!loopd_stop){
		temp = get_temp(global_temp_sources);
		set_rule_by_temp(global_config,temp);
		//fprintf(stderr,"Setting temp %d \n",temp);
		nanosleep(&ts,NULL);
	}

}
void loopd_exit(int s){
	loopd_stop = s;
}

void exitd(int s){
    	
    	fprintf(stderr,"Handling exit signal: %d \n",s);
	
	if(global_config){
		if(!ec_status){
			fprintf(stderr,"Setting fans to auto... \n");
		 	set_auto(global_config,FAN_ALL);
			close_ec();
		}
		free_device_config(global_config);
	}

	if(global_temp_sources){
		free_temp_sources(global_temp_sources);

	}
	
	if(pid_fd!=-1){
		lockf(pid_fd,F_ULOCK,0);
		close(pid_fd);
	}
	if(s == SIGTERM){
		exit(EXIT_SUCCESS);
	}else{
		exit(s);
	}
}

int main(int argc, char** argv){
	static const char* def_conf_path = "/etc/ecfc.conf";
	device_config current;
	temp_sources def_temp_sources;
	int error; 
	
	signal(SIGTERM, loopd_exit);
	signal(SIGINT, loopd_exit);
	
	initd();
	
	if(argc==2){
		error = parse(argv[1],&current);
	}else{
		error = parse(def_conf_path,&current);
	}

	if(error<0){
		fprintf(stderr,"Error opening config file. Exiting... \n");
		exitd(EXIT_FAILURE);
	}else if(error){
		fprintf(stderr,"Error parsing config file at line %d. Exiting... \n",error);
		exitd(EXIT_FAILURE);
	}else{
		global_config = &current;
		ec_status = init_ec();
		
		if(ec_status==-2){
			fprintf(stderr,"Error setting up EC: Secure Boot enabled. Exiting... \n");
			exitd(EXIT_FAILURE);
		}else if(ec_status<0){
			fprintf(stderr,"Error setting up EC: %s. Exiting... \n",strerror(errno));
			exitd(EXIT_FAILURE);
		}
	}

	fprintf(stderr,"Detecting temperature sources... \n");
	detect_temp_sources(&def_temp_sources);
	fprintf(stderr,"%ld temperature sources detected. \n",def_temp_sources.size);
	global_temp_sources = &def_temp_sources;

	loopd();
	
	exitd(loopd_stop);
}
