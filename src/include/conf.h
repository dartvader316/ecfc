#ifndef CONF_H
#define CONF_H

#include <stdbool.h>
#include <stdint.h>
#include <string.h>

#include "ec.h"

typedef struct{
	char* name;
	int up_temp;
	int down_temp;
	uint8_t speed;
} fan_rule;

enum write_occasions {
	ON_INIT,
};

typedef struct{
	char* name;
	uint8_t reg;
	uint8_t value;

	int write_occasion;
	
	bool reset_required;
	uint8_t reset_value;
} control_reg;

typedef struct{
	char* name;

	uint8_t read_reg;
	uint8_t write_reg;
	
	bool reset_required;
	uint8_t reset_value;
	
	uint8_t min_speed;
	uint8_t max_speed;
	
	fan_rule* fan_rules;
	size_t fan_rules_size;

	ssize_t current_rule;
} fan;

typedef struct{
	char* model;
	uint8_t critical_temp;
	int32_t ec_poll_interval;

	control_reg* control_regs;
	size_t control_regs_size;
	fan* fans;
	size_t fans_size;

} device_config;

extern void free_control_reg(control_reg* reg);
extern void free_fan(fan* f);
extern void free_device_config(device_config* c);

#endif
