#ifndef TEMP_H
#define TEMP_H

#include <stddef.h>

typedef struct {
	char** paths;
	size_t size;
} temp_sources;

void detect_temp_sources(temp_sources* temp_s);
void free_temp_sources(temp_sources* temp_s);

int get_max_temp(const temp_sources* temp_s);

int get_avg_temp(const temp_sources* temp_s);

int get_temp(const temp_sources* temp_s);


#endif
