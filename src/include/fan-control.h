#ifndef FAN_CONTROL_H

#include <stdbool.h>
#include "conf.h"

#define FAN_ALL -1

void set_rule_by_temp(const device_config* config,int temp);

void set_manual(const device_config* config,ssize_t fan_id);
void set_auto(const device_config* config,ssize_t fan_id);
bool is_manual(const device_config* config,ssize_t fan_id);

void set_fan_speed(const device_config* config,ssize_t fan_id,uint8_t speed);
uint8_t get_fan_speed(const device_config* config,ssize_t fan_id);

void set_fan_speed_percent(const device_config* config,ssize_t fan_id,int value);
uint8_t get_fan_speed_percent(const device_config* config,ssize_t fan_id);
#endif
