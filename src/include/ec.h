#ifndef EC_H
#define EC_H

#include <sys/stat.h>
#include <fcntl.h>
#include <stdint.h>
#include <unistd.h>

int init_ec();

extern void close_ec();

extern uint8_t read_ec(uint8_t offset);

extern void write_ec(uint8_t offset,uint8_t value);

#endif
