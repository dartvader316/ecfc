#include <stdio.h>
#include <stdlib.h>

#include "ec.h"
#include "conf.h"
#include "temp.h"
#include "conf-parser.h"
#include "fan-control.h"


void dump_fan(const fan* fan,const char* prefix){
	static const char* nullprefix = "";
	if(!prefix) prefix = nullprefix;
	printf("%sRead register: %d \n",prefix,fan->read_reg);
	printf("%sWrite register: %d \n",prefix,fan->write_reg);
	printf("%sReset required: %d \n",prefix,fan->reset_required);
	printf("%sReset value: %d \n",prefix,fan->reset_value);
	printf("%sMin speed: %d \n",prefix,fan->min_speed);
	printf("%sMax speed: %d \n",prefix,fan->max_speed);
	
	printf("%s#Fan rule \tUp \tDown \tSpeed \n",prefix); 
	for(size_t i =0;i<fan->fan_rules_size;i++){
		fan_rule* fr = fan->fan_rules + i;
		printf("%s%s \t%d \t%d \t%d\n",prefix,fr->name,fr->up_temp,fr->down_temp,fr->speed);
	}
}
void dump_control_register(const control_reg* reg,const char* prefix){
	static const char* nullprefix = "";
	if(!prefix) prefix = nullprefix;
	printf("%sRegister: %d \n",prefix,reg->reg);
	printf("%sValue: %d \n",prefix,reg->value);
	printf("%sWrite occasion: %d \n",prefix,reg->write_occasion);
	printf("%sReset required: %d \n",prefix,reg->reset_required);
	printf("%sReset value: %d \n",prefix,reg->reset_value);
}

void dump_device_config(const device_config* config){
	printf("Model: %s \n",config->model);
	
	printf("EC POLL INTERVAL: %d \n",config->ec_poll_interval);
	printf("Critical temp: %d \n",config->critical_temp);
	for(size_t i = 0;i<config->fans_size;i++){
		printf("Fan %s: \n",config->fans[i].name);
		dump_fan(&config->fans[i],"\t");
	}

	for(size_t i = 0;i<config->control_regs_size;i++){
		printf("Control Register %s: \n",config->control_regs[i].name);
		dump_control_register(&config->control_regs[i],"\t");
	}
}


void test_new(const char* config_path){

	int wait_time = 10;
	device_config current;
	int perr = parse(config_path,&current);

	if(perr!= 0){
		printf("Error in creating config: %d \n",perr);
		exit(-1);
	}
	
	puts("*****  CURRENT CONFIG START  *****");
	dump_device_config(&current);
	puts("*****  CURRENT CONFIG END    *****");
	
	if(!init_ec()){
		
		puts("Setting fans control to manual...");

		set_manual(&current,FAN_ALL);

		puts("Setting fans speed to max value...");
		
		set_fan_speed_percent(&current,FAN_ALL,100);
		
		printf("Waiting %d seconds for you to notice if fans become loud... \n",wait_time);
		sleep(wait_time);
		
		puts("Setting fans control to auto...");
		
		set_auto(&current,FAN_ALL);
		
		close_ec();
	}
	free_device_config(&current);
	
	perror("Test result");

}

int main(int argc, char** argv){
	const char* def_config = "/etc/ecfc.conf";
	
	if(argc==2){
		test_new(argv[1]);
	}else if(access(def_config,F_OK) == 0){
		test_new(def_config);
	}else{
		printf("Usage: %s [path to .conf] \n",argv[0]);
	}
}

