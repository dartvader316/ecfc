#include <stdio.h>
#include <stdlib.h>

#include "ec.h"
#include "conf.h"
#include "temp.h"
#include "conf-parser.h"
#include "fan-control.h"

void test(const char* config_path){

	device_config current;

	if(parse(config_path,&current)){
		printf("Error in parsing config: %s \n",config_path);
		exit(-1);
	}
	temp_sources def_temp_sources;
	detect_temp_sources(&def_temp_sources);

	int temp;
	if(!init_ec()){
		bool loop = true;
		
		set_manual(&current,FAN_ALL);
		
		while(loop){
			puts("Input constants:");
			puts("-2 -> exit");
			puts("-1 -> fan status");
			puts(" 0 -> autodetect");
			printf("Input temperature number:");
			scanf("%d",&temp);

			switch(temp){
				case -2: 
					loop = false;
					break;
				case -1: 
					puts("Current fan speeds:");
					for(size_t i = 0;i<current.fans_size;i++){
						printf("%s : %d%% (%d). Rule: %s (%ld)\n",
								current.fans[i].name,
								get_fan_speed_percent(&current,i),
								get_fan_speed(&current,i),
								current.fans[i].fan_rules[current.fans[i].current_rule].name,
								current.fans[i].current_rule
								);
					}
					break;
				case 0:
					temp = get_temp(&def_temp_sources);
					set_rule_by_temp(&current,temp);
					break;
				default:
					set_rule_by_temp(&current,temp);
			}
			printf("Temp %d is set \n",temp);
		}

		set_auto(&current,FAN_ALL);
		close_ec();
	}
	
	free_device_config(&current);
	
	perror("Test result");

}

int main(int argc, char** argv){
	const char* def_config = "/etc/ecfc.conf";
	
	if(argc==2){
		test(argv[1]);
	}else if(access(def_config,F_OK) == 0){
		test(def_config);
	}else{
		printf("Usage: %s [path to .conf] \n",argv[0]);
	}
}
