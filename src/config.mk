ECFC_ROOT ?= $(PWD)

CFLAGS+=-I$(ECFC_ROOT)/include -std=c99 -Wall -Wextra -Werror -pedantic

PATH_CORE=$(ECFC_ROOT)/core
LIB_CORE=core.o

PREFIX=/usr/local


# Do not use
EXTLIBS_CFLAGS = -D __SB_CHECK -I/usr/include/efivar -lefivar
GCC_TOOMANYWARNINGS_CFLAGS = -Wcast-align -Wcast-qual -Wdisabled-optimization -Wformat=2 -Winit-self -Wlogical-op -Wmissing-include-dirs -Wredundant-decls -Wshadow -Wstrict-overflow=5 -Wundef -fdiagnostics-show-option -Wconversion
