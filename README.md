# Ecfc
Embedded controller fan control for linux.
## Purpose 
I wanted a simple fan auto control daemon for modern laptops written in C which doesnt have bloated client-service architecture and have general conf style config, not xml or json.

## Installation

Install instructions are located [here](/%2E%2E/wikis/Installation)

## Configuration
Daemon tries to read /etc/ecfc.conf file by default.

Example of working config is located [here](/configs/xiaomi_mi_book.conf)

If your device have nbfc config, you can convert it to ecfc.conf with [nbfc2ecfc.py](/configs/nbfc2ecfc.py)
