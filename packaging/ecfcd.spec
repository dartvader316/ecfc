Name:       ecfcd
Version:    0.1
Release:    1
Summary:    Embedded controller fan control
License:    MIT

Source:     %{expand:%%(pwd)}
%description
Embedded controller fan control

%prep
cp -r %{SOURCEURL0}/../src %{_sourcedir}
mkdir -p %{_sourcedir}/packaging # IDK WHY BUT WITHOIT IT BUILD FAILED

%build

%install
mkdir -p %{buildroot}/%{_bindir}
make PREFIX=/usr DESTDIR=%{buildroot} ECFC_ROOT=%{_sourcedir} -C %{_sourcedir} release
make PREFIX=/usr DESTDIR=%{buildroot} ECFC_ROOT=%{_sourcedir} -C %{_sourcedir} install-systemd

%files
/usr/bin/ecfcd
/etc/systemd/system/ecfcd.service
/etc/systemd/system/ecfcd-sleep.service

%changelog
# Inital rpm support
